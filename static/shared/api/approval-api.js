import {invoke} from '@forge/bridge';

export function fetchApprovalsForContent() {
    return invoke("getContentApprovals");
}

export function approveContent() {
    return invoke("approveContent");
}

export function disapproveContent() {
    return invoke("disapproveContent");
}
