import React, {useEffect, useState} from 'react'
import {getContext} from "@forge/bridge/out/view/getContext";
import {SkeletonItem} from "@atlaskit/menu";

export const AtlassianContext = React.createContext(null);
export const AtlassianContextProvider = ({children}) => {
    const [forgeContext, setForgeContext] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetch = async () => {
            try {
                const forgeContext = await getContext();
                setForgeContext(forgeContext);
            } catch (e) {
                console.warn("AtlassianContextProvider could not get context information. Some features might not work correctly.");
                console.warn(e);
            } finally {
                setLoading(false);
            }
        }
        fetch();
    }, []);

    if (loading) {
        return <>
            {[1, 2].map(_ =>
                <SkeletonItem isShimmering
                              cssFn={css => ({...css, padding: 0, "min-height": "30px", "border-radius": "0"})}/>
            )}
        </>
    }

    return (
        <AtlassianContext.Provider
            value={{loading, forgeContext}}>
            {typeof children === "function" ? children({
                loading,
                forgeContext,
            }) : children}
        </AtlassianContext.Provider>
    )
}
