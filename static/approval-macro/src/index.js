import React from 'react';
import ReactDOM from 'react-dom';
import '@atlaskit/css-reset';
import App from "./App";
import {AtlassianContextProvider} from "../../shared/AtlassianContext";

ReactDOM.render(
    <React.StrictMode>
        <AtlassianContextProvider>
            <App/>
        </AtlassianContextProvider>
    </React.StrictMode>,
    document.getElementById('root')
);
