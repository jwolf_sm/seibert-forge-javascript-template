import React, {useContext, useEffect, useMemo, useState} from 'react';
import {LoadingButton} from "@atlaskit/button";
import styled from "styled-components";

export function ApproveControl({name, onApprove}) {
    const [isLoading, setIsLoading] = useState(false);
    const onBtnClick = () => {
        setIsLoading(true);
        onApprove().finally(() => {
            setIsLoading(false);
        })
    };
    const ControlWrapper = styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    `;
    return (
        <ControlWrapper>
            <div><b>{name}</b> is not approved.</div>
            <LoadingButton appearance="primary" onClick={onBtnClick} isLoading={isLoading}>Approve</LoadingButton>
        </ControlWrapper>

    )
}
