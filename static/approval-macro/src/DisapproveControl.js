import React, {useContext, useEffect, useMemo, useState} from 'react';
import Button, {LoadingButton} from "@atlaskit/button";
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import styled from "styled-components";

export function DisapproveControl({approval, onDisapprove}) {
    const [isLoading, setIsLoading] = useState(false);
    const onBtnClick = () => {
        setIsLoading(true);
        onDisapprove().finally(() => {
            setIsLoading(false);
        })
    };
    const ControlWrapper = styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    `;
    return (
        <ControlWrapper>
                <SuccessIcon primaryColor={"#36B37E"} label="Success"/>
                <div><b>{approval.name}</b> is approved by accountId {approval.accountId}</div>
            <LoadingButton onClick={onBtnClick} isLoading={isLoading} spacing="none"
                           appearance={"subtle-link"}>Disapprove</LoadingButton>
        </ControlWrapper>

    )
}
