import "./App.css";
import React, {useContext, useEffect, useMemo, useState} from 'react';
import {approveContent, disapproveContent, fetchApprovalsForContent} from "../../shared/api/approval-api";
import {AtlassianContext} from "../../shared/AtlassianContext";
import {ApproveControl} from "./ApproveControl";
import {DisapproveControl} from "./DisapproveControl";
import {SkeletonItem} from "@atlaskit/menu";

function App() {
    const atlassianContext = useContext(AtlassianContext);
    const [contentApprovals, setContentApprovals] = useState([]);
    const [isApprovalsLoading, setIsApprovalsLoading] = useState(true);
    const approval = useMemo(() => !atlassianContext.loading && !isApprovalsLoading && contentApprovals.find(approval => approval.name === atlassianContext.forgeContext.extension?.config?.name), [isApprovalsLoading, contentApprovals, atlassianContext.loading])

    useEffect(() => {
        loadApprovals();
    }, []);

    const loadApprovals = () => fetchApprovalsForContent()
        .then(approvals => {
            setContentApprovals(approvals);
        })
        .catch(() => {
            // TODO: error handling
        })
        .finally(() => {
            setIsApprovalsLoading(false);
        });

    const onApprove = async () => {
        await approveContent(); // TODO: error handling
        await loadApprovals();
    }

    const onDisapprove = async () => {
        await disapproveContent(); // TODO: error handling
        await loadApprovals();
    }

    if (isApprovalsLoading) {
        return <>
            {[1, 2].map(_ =>
                <SkeletonItem isShimmering
                              cssFn={css => ({...css, padding: 0, "min-height": "30px", "border-radius": "0"})}/>
            )}
        </>
    }

    return (
        (
            <div>
                {!isApprovalsLoading && !approval &&
                <ApproveControl name={atlassianContext.forgeContext.extension?.config?.name}
                                onApprove={onApprove}/>
                }
                {!isApprovalsLoading && approval &&
                <DisapproveControl approval={approval} onDisapprove={onDisapprove}/>}
            </div>
        )
    );
}


export default App;
