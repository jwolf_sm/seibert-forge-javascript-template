import "./App.css";
import React, {useEffect, useState} from 'react';
import {fetchApprovalsForContent} from "../../shared/api/approval-api";

function App() {
    const [contentApprovals, setContentApprovals] = useState([]);
    const [isApprovalsLoading, setIsApprovalsLoading] = useState(true);

    useEffect(() => {
        loadApprovals();
    }, []);
    const loadApprovals = () => fetchApprovalsForContent()
        .then(approvals => {
            setContentApprovals(approvals);
        })
        .catch(() => {
            // TODO: error handling
        })
        .finally(() => {
            setIsApprovalsLoading(false);
        });
    return (
        <div>
            Approval Overview Macro (fetched data: {JSON.stringify(contentApprovals)})
        </div>
    );
}


export default App;
