const { removeModuleScopePlugin } = require('customize-cra')
const { override, addExternalBabelPlugins } = require('customize-cra')

module.exports = override(
    removeModuleScopePlugin(),
    ...addExternalBabelPlugins(
        '@babel/plugin-proposal-nullish-coalescing-operator',
        '@babel/plugin-proposal-optional-chaining',
        '@babel/plugin-syntax-optional-chaining',
        '@babel/plugin-transform-react-jsx'
    )
)
