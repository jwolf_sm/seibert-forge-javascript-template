import {approvalSchema} from "../../shared/schemes/approval-schema";

test("approvalSchema rejects unknown prop", () => {
    const {error} = approvalSchema.validate( {"x": 1,"accountId":"60041ae4e2a13500694e7982","status":"approved"});
    expect(error.message).toBe("\"x\" is not allowed");
});
