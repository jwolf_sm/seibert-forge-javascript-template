import Joi from "joi";

export const approvalSchema = Joi.object({
    name: Joi.string(),
    accountId: Joi.string(),
    createdUtc: Joi.number(),
    status: Joi.string().allow("approved")
}).unknown(false);

export const approvalsSchema = Joi.array().items(approvalSchema).unique((a, b) => a.name === b.name);
