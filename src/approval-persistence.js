import {startsWith, storage} from "@forge/api";
import {approvalSchema, approvalsSchema} from "../shared/schemes/approval-schema";
import Joi from "joi";

function buildApprovalKeyForContentId({contentId}) {
    return `content-approvals-${contentId}`;
}

export function getApprovalsForContent({contentId}) {
    const key = buildApprovalKeyForContentId({contentId});
    console.log("get approvals for content", key);
    return storage.get(key);
}

function setApprovalsForContent({contentId, approvals}) {
    Joi.assert(approvals, approvalsSchema);
    const key = buildApprovalKeyForContentId({contentId});
    return storage.set(key, approvals);
}

export async function appendApprovalOnContent({contentId, approval}) {
    Joi.assert(approval, approvalSchema);
    const approvalsForContent = await getApprovalsForContent({contentId});
    if (Array.isArray(approvalsForContent)) {
        approvalsForContent.push(approval);
        return setApprovalsForContent({
            contentId,
            approvals: approvalsForContent.filter((approval, idx) => approvalsForContent.findIndex(approval2 => approval.name === approval2.name) === idx),
        });
    }
    return setApprovalsForContent({contentId, approvals: [approval]});
}

export async function deleteAccountsApprovalOnContent({contentId, accountId}) {
    const approvalsForContent = await getApprovalsForContent({contentId});
    if (!Array.isArray(approvalsForContent)) {
        return;
    }
    return setApprovalsForContent({
        contentId,
        approvals: approvalsForContent.filter(approval => approval.accountId !== accountId)
    });
}
