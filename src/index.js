import Resolver from '@forge/resolver';
import ForgeUI, {render} from "@forge/ui";
import {ApprovalMacroConfig} from "./approval-macro-config";
import {appendApprovalOnContent, deleteAccountsApprovalOnContent, getApprovalsForContent} from "./approval-persistence";
const resolver = new Resolver();

resolver.define("approveContent", async (req) => {
    const contentId = req.context.extension?.content?.id;
    const accountId = req.context.accountId;
    const approvalName = req.context.extension?.config?.name;
    if (!contentId || !accountId || !approvalName) {
        return;
    }
    const approval = {
        name: approvalName,
        accountId,
        status: "approved",
        createdUtc: (new Date()).getTime(),
    };
    await appendApprovalOnContent({contentId, approval});
})

resolver.define("disapproveContent", async (req) => {
    const contentId = req.context?.extension?.content?.id;
    const accountId = req.context.accountId;
    if (!contentId || !accountId) {
        return;
    }
    await deleteAccountsApprovalOnContent({contentId, accountId});
})

resolver.define("getContentApprovals", async (req) => {
    const contentId = req.context?.extension?.content?.id;
    if (!contentId) {
        return [];
    }
    return (await getApprovalsForContent({contentId})) || [];
})

export const handler = resolver.getDefinitions(); // exports backend function
export const approvalMacro = render(<ApprovalMacroConfig/>); // exports macro config ssr
