import ForgeUI, {MacroConfig, TextField} from '@forge/ui';

export const ApprovalMacroConfig = () => {
    return (
        <MacroConfig>
            {/* Form components */}
            <TextField name="name" label="Approval name" isRequired/>
        </MacroConfig>
    );
};
