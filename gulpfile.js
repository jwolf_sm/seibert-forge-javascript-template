const gulp = require('gulp')
const {series} = require('gulp')
const shell = require('gulp-shell')

gulp.task('build-approval-macro', shell.task('cd ./static/approval-macro && rm -rf ./build && npm run build'))
gulp.task('build-approval-overview-macro', shell.task('cd ./static/approval-overview-macro && rm -rf ./build && npm run build'))
gulp.task('run-tests', shell.task('npm run test'))
gulp.task('git-check', shell.task(`
if [[ $(git diff --stat) != '' ]]; then
  exit 125
else
  echo 'Git status is clean'
fi
`))
gulp.task('build', series(['git-check', 'run-tests', 'build-approval-macro', 'build-approval-overview-macro']))
