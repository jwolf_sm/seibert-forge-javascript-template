# Atlassian Forge Project Template on JavaScript

This a project template which can be used with Atlassians Forge CLI to bootstrap a new project. It implements a use-case
where users can give content approvals using Confluence page macros.

## Requirements

1. Ensure that you are using Node version 14.x and npm version 7.x. Older versions may cause errors.
1. [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) according to Atlassians official
   instructions.
1. Have a Confluence (or Jira) Cloud instance ready for testing purposes.
1. Be sure to have Docker installed and running to use the Forge tunnel.

## Quick Start

1. You can use this template by passing the bitbucket.org url of the repo to Forge cli.

```shell 
forge create --template '../jwolf_sm/seibert-forge-javascript-template' -d 'my-new-forge-project-dir' 
```

1. Visit the newly created template
2. Open "manifest.yml" and rename the second macro title (line 16). For example "second-test-macro". This is needed as
   Forge templating mechanism makes a mistake by giving both macros the same title. This does not work.
3. Install the dependencies

```shell 
npm i 
```

4. Build the source files

```shell 
npm run build 
```

5. Deploy the source files

```shell 
forge deploy
```

6. Install the app to your atlassian dev site.

```shell 
forge install
```

## Dev mode

The template contains two macros for example purposes. Change to their folders and start the development server.

```shell 
cd static/approval-macro/ && npm run start
cd static/approval-overview-macro/ && npm run start
```

React will automatically open the dev-servers homepage in a new tab. An error will show up there ("TypeError: Cannot
read property 'callBridge' of undefined"). That's fine. When both servers are running (they will use port 3000 and 3001)
you can start the Forge tunnel.

```shell 
forge tunnel
```
